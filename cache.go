package main

import (
	"sync"
)

// Cache holds the cached values
type Cache struct {
	lock  sync.RWMutex
	items map[string]interface{}
}

// Set is used to set the value in the cache
func (c *Cache) Set(key string, object interface{}) {
	c.lock.Lock()
	c.items[key] = object
	c.lock.Unlock()
}

// Get is used to get the value of a key from the cache
// Returned value needs to be type asserted
func (c *Cache) Get(key string) (interface{}, bool) {
	c.lock.RLock()
	obj, found := c.items[key]
	c.lock.RUnlock()
	return obj, found
}

// Invalidate is used to remove a key from the cache
func (c *Cache) Invalidate(key string) {
	c.lock.Lock()
	defer c.lock.Unlock()
	delete(c.items, key)
}

// Purge is used to completely clear the cache
func (c *Cache) Purge() {
	c.lock.Lock()
	defer c.lock.Unlock()
	c.items = make(map[string]interface{})
}

// NewCache initializes and returns a Cache
func NewCache() *Cache {
	return &Cache{items: make(map[string]interface{})}
}
