package main

import (
	"github.com/stretchr/testify/assert"
	"log"
	"os"
	"strconv"
	"testing"
)

func TestMain(m *testing.M) {
	// Initialize test database
	db, err := InitDB("sqlite3", "test.sqlite")
	if err != nil {
		log.Println(err)
	}
	defer db.Close()

	os.Exit(m.Run())
}

func TestPage(t *testing.T) {
	assert := assert.New(t)

	// Add page
	page := &ClinicPage{Title: "test page", Clinics: []int{1, 2, 3, 4}}
	err := AddPage(page)
	assert.NoError(err)
	assert.Equal(page.Id, int64(1))
	assert.Equal(page.Slug, "test-page")

	// Try adding same page again
	err = AddPage(page)
	assert.Error(err, "AddPage UNIQUE constraint failed: pages.slug")

	// Get page
	page, err = GetPage("test-page")
	assert.NoError(err)
	assert.Equal(page.Id, int64(1))
	assert.Equal(page.Title, "test page")
	assert.Equal(page.Clinics, []int{1, 2, 3, 4})
}

func TestComment(t *testing.T) {
	assert := assert.New(t)

	// Add comment
	comment := &ClinicComment{Clinic: 1, Name: "John Doe", Email: "johndoe@email.com", Comment: "Test Comment #1"}
	err := AddComment(comment)
	assert.NoError(err)
	assert.Equal(comment.Id, int64(1))
	// Add another comment
	comment.Comment = "Test Comment #2"
	err = AddComment(comment)
	assert.NoError(err)
	assert.Equal(comment.Id, int64(2))

	// Get comments
	comments, err := GetComments("1")
	assert.NoError(err)
	assert.Len(comments, 2)

}

func TestTop5Pages(t *testing.T) {
	assert := assert.New(t)

	// Add more unique pages
	page := &ClinicPage{}
	for i := 2; i < 8; i++ {
		page.Title = "test page " + strconv.Itoa(i)
		page.Clinics = []int{i}
		err := AddPage(page)
		assert.NoError(err)
		assert.Equal(page.Id, int64(i))
		assert.Equal(page.Slug, "test-page-"+strconv.Itoa(i))
	}

	// Get top 5 pages
	pages, err := GetTop5Pages()
	assert.NoError(err)
	if assert.Len(pages, 5) {
		assert.Equal(pages[0].CommentCount, 10)
		assert.Equal(pages[1].CommentCount, 7)
		assert.Equal(pages[2].CommentCount, 6)
		assert.Equal(pages[3].CommentCount, 5)
		assert.Equal(pages[4].CommentCount, 4)
	}

}
