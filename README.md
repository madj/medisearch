# MediSearch

MediSearch provides a clean and simple AngularJS front-end to display, sort and filter through a list of clincs along with an API to add pages, comments and get a list of top 5 pages with most comments

## Front-End Demo

https://medigo.herokuapp.com

## API

#### Add Page with Clinics

`POST` [/api/page](https://medigo.herokuapp.com/api/page)

`Request`

```json
{
  "title": "Clinics in Berlin",
  "clinics": [1, 2, 3]
}
```

`Response`

```json
{
  "id":1,"title":"Clinics in Berlin",
  "slug":"clinics-in-berlin","clinics": [1, 2, 3]
}
```

#### Get Page with Clinics

GET [/api/page/{slug}](https://medigo.herokuapp.com/api/page/clinics-in-berlin)

`Response`

```json
{
  "id": 1,
  "title": "Clinics in Berlin",
  "slug": "clinics-in-berlin",
  "clinics": [1, 2, 3]
}
```

#### Add Comment to a Clinic

`POST` [/api/clinic/{id}/comment](https://medigo.herokuapp.com/api/clinic/1/comment)

`Request`

```json
{
  "name": "John Doe",
  "email": "johndoe@email.com",
  "comment": "Great doctors. Awesome staff!"
}
```

`Response`

```json
{
  "id": 1,
  "clinic": 1,
  "name": "John Doe",
  "email": "johndoe@email.com",
  "comment": "Great doctors. Awesome staff!"
}
```

#### Get All Comments for a Clinic

GET [/api/clinic/{id}/comments](https://medigo.herokuapp.com/api/clinic/1/comments)

`Response`

```json
[
  {
    "clinic": 1,
    "name": "John Doe",
    "email": "johndoe@email.com",
    "comment": "Great doctors. Awesome staff!"
  }
]
```

#### Get Top 5 Pages with Most Comments for a Clinic

GET [/api/top5](https://medigo.herokuapp.com/api/top5)

`Response`

```json
[
  {
    "id": 5,
    "title": "New clinics",
    "slug": "new-clinics",
    "comment_count": 13
  },
  {
    "id": 2,
    "title": "Clinics in Germany",
    "slug": "clinics-in-germany",
    "comment_count": 12
  },
  {
    "id": 6,
    "title": "Clinics that allow dogs",
    "slug": "clinics-that-allow-dogs",
    "comment_count": 11
  },
  {
    "id": 4,
    "title": "Best voted clinics 2014",
    "slug": "best-voted-clinics-2014",
    "comment_count": 10
  },
  {
    "id": 7,
    "title": "Clinics with Arabic speaking Doctors",
    "slug": "clinics-with-arabic-speaking-doctors",
    "comment_count": 7
  }
]
```
