package main

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/gosimple/slug"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"os"
)

var db *sql.DB
var dbcache *Cache

func InitDB(driver, dbname string) (*sql.DB, error) {
	var err error
	createDb := false
	if _, err := os.Stat(dbname); os.IsNotExist(err) {
		log.Printf("InitDB: database %s does not exist", dbname)
		createDb = true
	}
	db, err = sql.Open(driver, dbname)
	if createDb {
		if err = syncDB(); err != nil {
			return nil, errors.New("Error creating database")
		}
	}
	dbcache = NewCache()
	return db, nil
}

func syncDB() (err error) {
	log.Println("syncDB")
	// Create pages table
	createPages := `CREATE TABLE pages (
                  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                  title TEXT NOT NULL,
                  slug TEXT UNIQUE NOT NULL,
                  created_at TIMESTAMP default CURRENT_TIMESTAMP,
                  updated_at TIMESTAMP
                );`
	_, err = db.Exec(createPages, nil)
	if err != nil {
		return err
	}
	// Create page_clinics junction table
	createPageClinics := `CREATE TABLE page_clinics (
                  page_id INTEGER NOT NULL REFERENCES pages(id),
                  clinic_id INTEGER NOT NULL
                );`
	_, err = db.Exec(createPageClinics, nil)
	if err != nil {
		return err
	}
	// Create comments table
	createComments := `CREATE TABLE comments (
                  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                  clinic_id INTEGER NOT NULL REFERENCES pages(id),
                  name TEXT NOT NULL,
                  email TEXT NOT NULL,
                  comment TEXT NOT NULL,
                  created_at TIMESTAMP default CURRENT_TIMESTAMP,
                  updated_at TIMESTAMP
                );`
	_, err = db.Exec(createComments, nil)
	return err
}

func AddPage(page *ClinicPage) error {
	log.Println("AddPage", page.Title)

	// Create url slug from page title
	// TODO: Create a unique slug if the page title is same
	// by checking with the database if slug exists
	page.Slug = slug.Make(page.Title)

	stmtAddPage, _ := db.Prepare("INSERT INTO pages(title, slug) VALUES(?, ?)")
	defer stmtAddPage.Close()

	result, err := stmtAddPage.Exec(page.Title, page.Slug)
	if err != nil {
		log.Println("AddPage", err.Error())
		return err
	}
	page.Id, _ = result.LastInsertId()

	// map clinics to page
	stmtLinkClinic, _ := db.Prepare("INSERT INTO page_clinics VALUES(?, ?)")
	defer stmtLinkClinic.Close()
	for _, clinicId := range page.Clinics {
		stmtLinkClinic.Exec(page.Id, clinicId)
	}
	// Invalidate top5 cache
	dbcache.Invalidate("top5")
	return nil
}

func GetPage(slug string) (*ClinicPage, error) {
	log.Println("GetPage", slug)
	query := fmt.Sprintf(`SELECT pages.id, pages.title, pages.slug, 
                        page_clinics.clinic_id FROM pages, page_clinics 
                        where page_clinics.page_id = pages.id AND slug="%v";`, slug)
	rows, err := db.Query(query)
	if err != nil {
		log.Println("GetPage", err)
		return nil, err
	}
	defer rows.Close()
	page := &ClinicPage{}
	var clinicId int
	for rows.Next() {
		rows.Scan(&page.Id, &page.Title, &page.Slug, &clinicId)
		page.Clinics = append(page.Clinics, clinicId)
	}
	return page, nil
}

func GetTop5Pages() (pages []ClinicPage, err error) {
	log.Println("GetTop5Pages")
	// Query for getting top 5 pages by actual comment count
	/*
	  SELECT page_id, COUNT(comments.id) as comment_count FROM page_clinics, comments
	  WHERE comments.clinic_id = page_clinics.clinic_id
	  GROUP BY page_id
	  ORDER BY comment_count DESC
	  LIMIT 0, 5;
	*/
	// check if available in cache
	obj, found := dbcache.Get("top5")
	if found {
		log.Println("Cache Hit")
		pages = obj.([]ClinicPage)
		return pages, nil
	}
	log.Println("Cache Miss")
	query := `SELECT page_id, title, slug, SUM(clinic_id) as comment_count FROM page_clinics, pages
            WHERE page_clinics.page_id = pages.id
            GROUP BY page_id
            ORDER BY comment_count DESC
            LIMIT 0, 5;`
	rows, err := db.Query(query)
	if err != nil {
		log.Println("GetTop5Pages", err)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var page ClinicPage
		rows.Scan(&page.Id, &page.Title, &page.Slug, &page.CommentCount)
		pages = append(pages, page)
	}
	// cache results
	dbcache.Set("top5", pages)
	return pages, nil
}

func AddComment(comment *ClinicComment) error {
	log.Println("AddComment", comment.Comment)
	stmtAddComment, _ := db.Prepare("INSERT INTO comments(clinic_id, name, email, comment) VALUES(?, ?, ?, ?)")
	defer stmtAddComment.Close()
	result, err := stmtAddComment.Exec(comment.Clinic, comment.Name, comment.Email, comment.Comment)
	if err != nil {
		log.Println("AddComment", err.Error())
		return err
	}
	comment.Id, _ = result.LastInsertId()
	return nil
}

func GetComments(clinicId string) (comments []ClinicComment, err error) {
	log.Println("GetComments")
	query := fmt.Sprintf(`SELECT clinic_id, name, email, comment FROM comments 
                        WHERE clinic_id="%v" ORDER BY created_at`, clinicId)
	rows, err := db.Query(query)
	if err != nil {
		log.Println("GetComments", err)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var comment ClinicComment
		rows.Scan(&comment.Clinic, &comment.Name, &comment.Email, &comment.Comment)
		comments = append(comments, comment)
	}
	return comments, nil
}
