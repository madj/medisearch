package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type TestStruct struct {
	Num int
}

func TestCache(t *testing.T) {
	assert := assert.New(t)
	tc := NewCache()

	// Test for Get() on empty
	a, found := tc.Get("a")
	assert.False(found, "Getting found value that shouldn't exist (#1)")
	assert.Nil(a)

	b, found := tc.Get("b")
	assert.False(found, "Getting found value that shouldn't exist (#2)")
	assert.Nil(b)

	c, found := tc.Get("c")
	assert.False(found, "Getting found value that shouldn't exist (#3)")
	assert.Nil(c)

	// Test for Set() then Get()
	tc.Set("a", 1)
	tc.Set("b", "b")
	tc.Set("c", 3.5)

	x, found := tc.Get("a")
	assert.True(found, "a was not found while getting")
	assert.NotNil(x)
	assert.Equal(x.(int)+2, 3)

	x, found = tc.Get("b")
	assert.True(found, "b was not found while getting")
	assert.NotNil(x)
	assert.Equal(x.(string)+"B", "bB")

	x, found = tc.Get("c")
	assert.True(found, "c was not found while getting")
	assert.NotNil(x)
	assert.Equal(x.(float64)+1.0, 4.5)

	// Test for Invalidate() then Get()
	tc.Invalidate("a")
	a, found = tc.Get("a")
	assert.False(found, "Getting found value that shouldn't exist (#4)")
	assert.Nil(a)

	x, found = tc.Get("b")
	assert.True(found, "b was not found while getting after invalidation")
	assert.NotNil(x)
	assert.Equal(x.(string)+"B", "bB")

	// Test for Purge() then Get()
	tc.Purge()
	b, found = tc.Get("b")
	assert.False(found, "Getting found value that shouldn't exist after purge (#5)")
	assert.Nil(b)

	c, found = tc.Get("c")
	assert.False(found, "Getting found value that shouldn't exist after purge (#6)")
	assert.Nil(c)
}

func TestStorePointerToStruct(t *testing.T) {
	assert := assert.New(t)
	tc := NewCache()

	tc.Set("foo", &TestStruct{Num: 1})
	x, found := tc.Get("foo")
	assert.True(found, "*TestStruct was not found for foo (#1)")

	foo := x.(*TestStruct)
	foo.Num++

	y, found := tc.Get("foo")
	assert.True(found, "*TestStruct was not found for foo (#2)")

	bar := y.(*TestStruct)
	assert.Equal(bar.Num, 2, "TestStruct.Num is not 2")
}
