package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

// ClinicPage structure is used to Marshal / Unmarshal
// data for AddPage, GetPage and GetTop5Pages
type ClinicPage struct {
	Id           int64  `json:"id,omitempty"`
	Title        string `json:"title"`
	Slug         string `json:"slug,omitempty"`
	Clinics      []int  `json:"clinics,omitempty"`
	CommentCount int    `json:"comment_count,omitempty"`
}

// ClinicComment structure is used to Marshal / Unmarshal
// data for AddComment and GetComments
type ClinicComment struct {
	Id      int64  `json:"id,omitempty"`
	Clinic  int64  `json:"clinic,omitempty"`
	Name    string `json:"name"`
	Email   string `json:"email"`
	Comment string `json:"comment"`
}

type JSONError struct {
	Error string `json:"error"`
}

func ErrorResponse(w http.ResponseWriter, err error) {
	jsonError := &JSONError{Error: err.Error()}
	toWrite, _ := json.Marshal(jsonError)
	w.WriteHeader(http.StatusInternalServerError)
	w.Write(toWrite)
}

func AddPageHandler(w http.ResponseWriter, r *http.Request) {
	var err error
	w.Header().Set("Content-type", "application/json")

	var page ClinicPage
	err = json.NewDecoder(r.Body).Decode(&page)
	if err != nil {
		ErrorResponse(w, err)
		return
	}

	err = AddPage(&page)
	if err != nil {
		ErrorResponse(w, err)
		return
	}
	pageJSON, err := json.Marshal(page)
	if err != nil {
		ErrorResponse(w, err)
		return
	}
	w.Write(pageJSON)
}

func GetPageHandler(w http.ResponseWriter, r *http.Request) {
	var err error
	w.Header().Set("Content-type", "application/json")

	vars := mux.Vars(r)
	slug := vars["slug"]
	page, err := GetPage(slug)
	if err != nil {
		ErrorResponse(w, err)
		return
	}
	pageJSON, err := json.Marshal(page)
	if err != nil {
		ErrorResponse(w, err)
		return
	}
	w.Write(pageJSON)
}

func AddCommentHandler(w http.ResponseWriter, r *http.Request) {
	var err error
	w.Header().Set("Content-type", "application/json")

	var comment ClinicComment
	vars := mux.Vars(r)
	comment.Clinic, err = strconv.ParseInt(vars["id"], 0, 64)
	if err != nil {
		ErrorResponse(w, err)
		return
	}
	err = json.NewDecoder(r.Body).Decode(&comment)
	if err != nil {
		ErrorResponse(w, err)
		return
	}
	err = AddComment(&comment)
	if err != nil {
		ErrorResponse(w, err)
		return
	}
	commentJSON, err := json.Marshal(comment)
	if err != nil {
		ErrorResponse(w, err)
		return
	}
	w.Write(commentJSON)
}

func GetCommentsHandler(w http.ResponseWriter, r *http.Request) {
	var err error
	w.Header().Set("Content-type", "application/json")

	vars := mux.Vars(r)
	clinicId := vars["id"]
	comments, err := GetComments(clinicId)
	if err != nil {
		ErrorResponse(w, err)
		return
	}
	commentsJSON, err := json.Marshal(comments)
	if err != nil {
		ErrorResponse(w, err)
		return
	}
	w.Write(commentsJSON)
}

func Top5Handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	pages, err := GetTop5Pages()
	if err != nil {
		ErrorResponse(w, err)
		return
	}
	pagesJSON, err := json.Marshal(pages)
	if err != nil {
		ErrorResponse(w, err)
		return
	}
	w.Write(pagesJSON)
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./templates/index.html")
}

// Serves the /assets/ directory for js, html, css assets
func AssetsHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	static := vars["path"]
	http.ServeFile(w, r, filepath.Join("./assets", static))
}

func main() {

	db, err := InitDB("sqlite3", "db.sqlite")
	if err != nil {
		log.Fatal(err.Error())
		return
	}
	defer db.Close()

	r := mux.NewRouter()
	r.StrictSlash(true)

	r.HandleFunc("/", HomeHandler).Methods("GET")
	r.HandleFunc(`/assets/{path:[a-zA-Z0-9=\-\/\.\_]+}`, AssetsHandler).Methods("GET")

	r.HandleFunc(`/api/page`, AddPageHandler).Methods("POST")
	r.HandleFunc(`/api/page/{slug:[a-zA-Z0-9=\-\/\.\_]+}`, GetPageHandler).Methods("GET")
	r.HandleFunc(`/api/clinic/{id:[0-9]+}/comment`, AddCommentHandler).Methods("POST")
	r.HandleFunc(`/api/clinic/{id:[0-9]+}/comments`, GetCommentsHandler).Methods("GET")
	r.HandleFunc(`/api/top5`, Top5Handler).Methods("GET")

	http.Handle("/", r)

	httpHost := os.Getenv("HOST")
	httpPort := os.Getenv("PORT")
	if httpPort == "" {
		httpPort = "8080"
	}
	bind := fmt.Sprintf("%s:%s", httpHost, httpPort)
	log.Println("listening on ", bind)
	http.ListenAndServe(bind, nil)
}
