'use strict';

angular.module('medisearch').controller('ClinicController', ['$scope', 'Clinic', function($scope, Clinic) {    
    $scope.clinics = [];
    $scope.selectedProcedures = [];
    $scope.availableProcedures = [];
    $scope.isLoading = true;
    $scope.predicate = 'name';
    
    Clinic.getAll().then(function(clinics) {
      $scope.clinics = clinics;
            
      angular.forEach(clinics, function(clinic) {
        angular.forEach(clinic.procedures, function(procedure) {
          if($scope.availableProcedures.indexOf(procedure.name) == -1) {
            $scope.availableProcedures.push(procedure.name);
          }
        });
      });
      $scope.isLoading = false;
    });
    
    $scope.filterByProcedure = function(clinic) {
      // if none of the checkboxes are selected, return all clinics
      if($scope.selectedProcedures.length == 0) { 
        return true;
      }
      // match selected check boxes with available clinics
      var found = false;
      for(var i = 0; i < clinic.procedures.length; i++) {
        // check if clinic procedure exists in selected filter
        if($scope.selectedProcedures.indexOf(clinic.procedures[i].name) !== -1) {
          found = true;
          break;
        }
      }
      return found;
    }
}]);
