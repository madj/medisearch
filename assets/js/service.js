'use strict';

var BASE_URL = 'https://private-6e916-medigochallenges.apiary-mock.com';

angular.module('medisearch')
.factory('Clinic', ['$http', '$q', function($http, $q) {
  return {
    getAll: function(procedure) {
     var deferred = $q.defer();     
     
     // check if a specific procedure is selected
     var clinicsURL = BASE_URL + '/clinics';
     if (typeof procedure !== 'undefined') {
      url += '?procedure=' + procedure;
     }
     
     // get all clinic ids
     $http.get(clinicsURL)
      .success(function(clinicIds) {      
        // fetch details of all the clinics
        var promises = [];
        for(var i = 0; i < clinicIds.length; i++) {
          promises.push($http.get(BASE_URL + '/clinics/' + clinicIds[i]));          
        }
        $q.all(promises).then(function(response) {
          var clinics = [];
          angular.forEach(response, function(resp) {
            this.push(resp.data);
          }, clinics);
          deferred.resolve(clinics);
        });
      })
      .error(function(err) {        
        deferred.reject(err);        
      });
      return deferred.promise;
    }
  };
}]);
