'use strict';

angular.module("medisearch").directive("starRating", function() {
  return {
    restrict : "A",
    template : "<ul class='rating'>" +
               "  <li ng-repeat='star in stars' ng-class='star'>" +
               "    <i class='ion-star'></i>" +
               "  </li>" +
               "</ul>",
    scope : {
      starRating : "="
    },
    link : function(scope, elem, attrs) {
      scope.stars = [];
      for (var i = 0; i < 10; i++) {
        scope.stars.push({
          filled : i < scope.starRating
        });
      }
    }
  };
});
